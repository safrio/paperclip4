class RemoveMaterialFromCourse < ActiveRecord::Migration[5.0]
  def change
    remove_reference :courses, :material
  end
end
