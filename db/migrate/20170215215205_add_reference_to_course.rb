class AddReferenceToCourse < ActiveRecord::Migration[5.0]
  def change
    add_reference :courses, :material, foreign_key: true
  end
end
