class PaperclipJob < ActiveJob::Base
  queue_as :default

  def perform(material_id)
    material = Material.find(material_id)
    material.upload_to_s3
  end
end
