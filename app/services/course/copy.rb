class Course::Copy
  def initialize(course_object)
    @course = course_object
    make_dup
  end

  def make_dup
    @course_dup = @course.dup
    @course_dup.save!
    @course.class.reflect_on_all_associations(:has_many).each do |association|
      case association.name
      when :materials
        @course.send(association.name).map { |material| material_save(material) }
      end
    end
  end

  def material_save(material)
    material_dup = material.dup
    material_dup.course = @course_dup
    material_dup.file = material.file
    material_dup.save!
  end
end

# m.file.instance_write(:file_name, 'temp.png')
# original_filename = original_material.file.original_filename
# # nanorandom filename
# new_filename = [Time.now().nsec, rand(99)].join("_")
# ext = File.extname(original_filename)

# m.file.reprocess!


#File.basename(original_filename, File.extname(original_filename)),
