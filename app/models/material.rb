class Material < ActiveRecord::Base
  belongs_to :course

  has_attached_file :file,
                    styles: { thumb: "100x>" },
                    default_url: "/images/:style/missing.png",
                    storage: :s3,
                    s3_credentials: #"#{Rails.root}/config/s3.yml",  
                    {
                      bucket:            Rails.configuration.service['bucket'],
                      access_key_id:     Rails.configuration.service['access_key_id'],
                      secret_access_key: Rails.configuration.service['secret_access_key']
                    },
                    :s3_region => 'eu-central-1',
                    :s3_permissions => 'public-read',
                    :use_default_time_zone => false,
                    :s3_host_name => 's3.eu-central-1.amazonaws.com'

  do_not_validate_attachment_file_type :file

  process_in_background :file

  # after_save :queue_upload_to_s3

  # def queue_upload_to_s3
  #   PaperclipJob.set(wait: 5.second).perform_later self.id
  # end

  # def upload_to_s3
  #   self.file = local_image.to_file
  #   save!
  # end

end
